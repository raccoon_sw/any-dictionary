package dictionary.translation;

import dictionary.entry.IDictionaryEntry;
import templates.translation.TranslationTemplate;

import java.util.ArrayList;
import java.util.List;

public class TranslationDictionaryEntry implements IDictionaryEntry {
    private String lemma;
    private List<Translation> definitions;

    public TranslationDictionaryEntry(String lemma) {
        this.lemma = lemma;
        this.definitions = new ArrayList<>();
    }

    public TranslationDictionaryEntry(String lemma, List<Translation> definitions) {
        this.lemma = lemma;
        this.definitions = definitions;
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public String getDictionaryTemplate() {
        return TranslationTemplate.getDictionaryEntry(new TranslationDictionaryEntry(lemma, definitions));
    }

    public List<Translation> getDefinitions() {
        return definitions;
    }

    public void setDefinitions(List<Translation> definitions) {
        this.definitions = definitions;
    }

}
