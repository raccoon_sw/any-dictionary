package dictionary.translation;

public class Pair<S, T> {
    private S language;
    private T translation;

    public Pair(S language, T translation) {
        this.translation = translation;
        this.language = language;
    }

    public T getTranslation() {
        return translation;
    }

    public void setTranslation(T translation) {
        this.translation = translation;
    }

    public S getLanguage() {
        return language;
    }

    public void setLanguage(S language) {
        this.language = language;
    }
}
