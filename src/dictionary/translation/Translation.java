package dictionary.translation;

import java.util.List;

public class Translation {
    private String definition;
    private List<Pair<String, String>> translations; // translate in language + language

    public Translation(String definition, List<Pair<String, String>> translations) {
        this.definition = definition;
        this.translations = translations;
    }

    public String getDefinition() {
        return definition;
    }

    public List<Pair<String, String>> getTranslations() {
        return translations;
    }

}
