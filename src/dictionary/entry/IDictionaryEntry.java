package dictionary.entry;

import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import java.io.IOException;

public interface IDictionaryEntry {
    String getLemma();

    void setLemma(String lemma);

    String getDictionaryTemplate() throws ResourceNotFoundException,
            ParseErrorException, MethodInvocationException, IOException;
}
