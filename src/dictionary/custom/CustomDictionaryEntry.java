package dictionary.custom;

import dictionary.entry.IDictionaryEntry;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import templates.custom.CustomTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomDictionaryEntry implements IDictionaryEntry {
    private String lemma;
    private List<List<String>> features;

    public CustomDictionaryEntry(String lemma) {
        this.lemma = lemma;
        this.features = new ArrayList<>();
    }

    public CustomDictionaryEntry(String lemma, List<List<String>> features) {
        this.lemma = lemma;
        this.features = features;
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public String getDictionaryTemplate() throws ResourceNotFoundException,
            ParseErrorException, MethodInvocationException, IOException {

        return CustomTemplate.getDictionaryEntry(new CustomDictionaryEntry(lemma, features));
    }

    public List<List<String>> getFeatures() {
        return features;
    }

    public void setFeatures(List<List<String>> features) {
        this.features = features;
    }

}
