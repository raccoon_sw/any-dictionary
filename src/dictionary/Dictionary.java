package dictionary;

import dictionary.entry.IDictionaryEntry;
import sparql.DictionaryQueryData;

import java.util.ArrayList;
import java.util.List;

public class Dictionary {
    private String type = "";
    private DictionaryQueryData queryData;
    private List<IDictionaryEntry> dictionaryEntries;

    public Dictionary() {
        this.dictionaryEntries = new ArrayList<>();
    }

    public Dictionary(DictionaryQueryData queryData) {
        this.queryData = queryData;
        this.dictionaryEntries = new ArrayList<>();
    }

    public String getType() {
        return type.compareTo("") == 0 ? queryData.getDictionaryType() : type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setQueryData(DictionaryQueryData queryData) {
        this.queryData = queryData;
    }

    public DictionaryQueryData getQueryData() {
        return queryData;
    }

    public List<IDictionaryEntry> getDictionaryEntries() {
        return dictionaryEntries;
    }

    public void setDictionaryEntries(List<IDictionaryEntry> dictionaryEntries) {
        this.dictionaryEntries = dictionaryEntries;
    }

}
