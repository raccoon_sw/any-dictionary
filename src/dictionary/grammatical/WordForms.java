package dictionary.grammatical;

import java.util.List;

public class WordForms {
    private String word;
    private List<String> lemmas;

    public WordForms(String word, List<String> lemmas) {
        this.word = word;
        this.lemmas = lemmas;
    }

    public String getWord() {
        return word;
    }

    public List<String> getLemmas() {
        return lemmas;
    }

}
