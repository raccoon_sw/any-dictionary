package dictionary.grammatical;

import dictionary.entry.IDictionaryEntry;
import enums.PartOfSpeech;
import org.javatuples.Pair;
import templates.grammatical.GrammaticalTemplate;

import java.util.ArrayList;
import java.util.List;

public class GrammaticalDictionaryEntry implements IDictionaryEntry {
    private String lemma;
    private Pair<PartOfSpeech, String> part;
    private String gender;
    private List<WordForms> wordForms;

    public GrammaticalDictionaryEntry(String lemma) {
        this.lemma = lemma;
        this.part = null;
        this.gender = "";
        this.wordForms = new ArrayList<>();
    }

    public GrammaticalDictionaryEntry(String lemma, Pair<PartOfSpeech, String> part, String gender, List<WordForms> wordForms) {
        this.lemma = lemma;
        this.part = part;
        this.gender = gender;
        this.wordForms = wordForms;
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public String getDictionaryTemplate() {
        return GrammaticalTemplate.getDictionaryEntry(
                new GrammaticalDictionaryEntry(lemma, part, gender, wordForms));
    }

    public Pair<PartOfSpeech, String> getPart() {
        return part;
    }

    public void setPart(Pair<PartOfSpeech, String> part) {
        this.part = part;
    }

    public String getGender() {
        return gender;
    }

    public List<WordForms> getWordForms() {
        return wordForms;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWordForms(List<WordForms> wordForms) {
        this.wordForms = wordForms;
    }

}
