package dictionary.explanatory;

import dictionary.entry.IDictionaryEntry;
import templates.explanatory.ExplanatoryTemplate;

import java.util.ArrayList;
import java.util.List;

public class ExplanatoryDictionaryEntry implements IDictionaryEntry {
    private String lemma;
    private List<String> senses;

    public ExplanatoryDictionaryEntry(String lemma) {
        this.lemma = lemma;
        this.senses = new ArrayList<>();
    }

    private ExplanatoryDictionaryEntry(String lemma, List<String> senses) {
        this.lemma = lemma;
        this.senses = senses;
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public String getDictionaryTemplate() {
        return ExplanatoryTemplate.getDictionaryEntry(new ExplanatoryDictionaryEntry(lemma, senses));
    }

    public List<String> getSenses() {
        return senses;
    }

    public void setSenses(List<String> senses) {
        this.senses = senses;
    }

}
