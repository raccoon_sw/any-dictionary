package app;

import frontend.controllers.MainViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class App extends Application {
    private static Properties properties = new Properties();

    public static Properties getProperties() {
        return properties;
    }

    private static void setStageSettings(Stage primaryStage, Parent parent) {
        primaryStage.setTitle("Словарь любого типа");
        primaryStage.getIcons().add(new Image(properties.getProperty("icoMainPath")));
        primaryStage.setScene(new Scene(parent));
        primaryStage.setMinHeight(680);
        primaryStage.setMinWidth(1107);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        FileInputStream input = new FileInputStream(new File("resources/config.properties"));
        properties.load(new InputStreamReader(input, StandardCharsets.UTF_8));

        URL url = getClass().getResource(properties.getProperty("mainViewPath"));
        FXMLLoader loader = new FXMLLoader(url);
        Parent parent = loader.load();

        MainViewController controller = loader.getController();
        setStageSettings(primaryStage, parent);
        controller.setPrimaryStage(primaryStage);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        //save current session
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
