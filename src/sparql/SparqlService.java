package sparql;

import dictionary.Dictionary;
import dictionary.entry.IDictionaryEntry;
import dictionary.custom.CustomDictionaryEntry;
import dictionary.explanatory.ExplanatoryDictionaryEntry;
import dictionary.grammatical.GrammaticalDictionaryEntry;
import dictionary.grammatical.WordForms;
import dictionary.translation.Pair;
import dictionary.translation.Translation;
import dictionary.translation.TranslationDictionaryEntry;
import enums.WordOrderType;
import org.apache.jena.atlas.json.JsonParseException;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;
import utils.wordsOrder.WordsOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SparqlService {
    private static String wikidata = "https://query.wikidata.org/sparql";

    private static void throwNotFoundException(ResultSet result) throws IllegalStateException {
        if (!result.hasNext()) {
            throw new IllegalStateException();
        }
    }

    public static Dictionary getResponseForAll(DictionaryQueryData dictionaryQueryData, WordOrderType wordOrderType)
            throws QueryExceptionHTTP, JsonParseException, IllegalStateException {
        Dictionary dictionary = new Dictionary(dictionaryQueryData);
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(wikidata,
                dictionaryQueryData.getQueryTextForAll());
        ResultSet result;
        result = queryExecution.execSelect();
        switch (dictionaryQueryData.getDictionaryType()) {
            case "Грамматический": {
                throwNotFoundException(result);
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    dictionary.getDictionaryEntries().add(new GrammaticalDictionaryEntry(QueryParser.ifNullWithReplace(
                            solution.get(dictionaryQueryData.getQueryFieldForAll()))));
                }
                dictionary.setType("Грамматический");
                break;
            }
            case "Толковый": {
                throwNotFoundException(result);
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    dictionary.getDictionaryEntries().add(new ExplanatoryDictionaryEntry(QueryParser.ifNullWithReplace(
                            solution.get(dictionaryQueryData.getQueryFieldForAll()))));
                }
                dictionary.setType("Толковый");
                break;
            }
            case "Переводной": {
                throwNotFoundException(result);
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    dictionary.getDictionaryEntries().add(new TranslationDictionaryEntry(QueryParser.ifNullWithReplace(
                            solution.get(dictionaryQueryData.getQueryFieldForAll()))));
                }
                dictionary.setType("Переводной");
                break;
            }
            default: {
                throwNotFoundException(result);
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    dictionary.getDictionaryEntries().add(new CustomDictionaryEntry(QueryParser.ifNullWithReplace(
                            solution.get(dictionaryQueryData.getQueryFieldForAll()))));
                }
                dictionary.setQueryData(dictionaryQueryData);
                break;
            }
        }
//        } catch (QueryBuildException qbe) {
//            qbe.printStackTrace();
////            QueryBuildException is exception for all exceptions during query execution construction.
//            System.out.println("QueryBuildException");
//        } catch (QueryCancelledException qce) {
//            qce.printStackTrace();
////            Indicate that a query execution has been cancelled and the operation can 't be called
//            System.out.println("QueryCancelledException");
//        } catch (QueryExecException qee) {
//            qee.printStackTrace();
////            QueryExecException indicates a condition encountered during query evaluation.
//            System.out.println("QueryExecException");
//        } catch (QueryFatalException qfe) {
//            qfe.printStackTrace();
////            QueryFatalException is such that the query aborts do to some
////            problem(this might be an internal error or something in the way the query builds or executes).
//            System.out.println("QueryFatalException");
//        } catch (QueryParseException qpe) {
//            qpe.printStackTrace();
//            System.out.println("QueryParseException");
//        } catch (ResultSetException qpe) {
//            qpe.printStackTrace();
//            System.out.println("ResultSetException");
//        } catch (ExprEvalException qpe) {
//            qpe.printStackTrace();
////            Exception for a dynamic evaluation exception.
//            System.out.println("ExprEvalException");
//        } catch (ExprException qpe) {
//            qpe.printStackTrace();
////            The root of all expression exceptions
//            System.out.println("ExprException");
//        } catch (QueryCheckException qpe) {
//            qpe.printStackTrace();
//            System.out.println("QueryCheckException");
//        }
        switch (wordOrderType) {
            case Lexicographical: {
                WordsOrder.setLexicographicalOrder(dictionary.getDictionaryEntries());
                break;
            }
            case Inverse: {
                WordsOrder.setInverseOrder(dictionary.getDictionaryEntries());
                break;
            }
        }
        return dictionary;
    }

    private static String addWordInQuery(String query, String searchWord) {
        return query.replace("needed_word", searchWord);
    }

    public static IDictionaryEntry getResponseForOne(IDictionaryEntry data, DictionaryQueryData dictionaryQueryData, String searchWord)
            throws QueryExceptionHTTP, JsonParseException, IllegalStateException {
        System.out.println(searchWord.compareTo("") == 0 ? data.getLemma() : searchWord);
        QueryExecution execution = QueryExecutionFactory.sparqlService(wikidata,
                addWordInQuery(dictionaryQueryData.getQueryTextForOne(),
                        searchWord.compareTo("") == 0 ? data.getLemma() : searchWord));
        ResultSet result = execution.execSelect();
        switch (dictionaryQueryData.getDictionaryType()) {
            case "Грамматический": {
                throwNotFoundException(result);
                List<GrammaticalDictionaryEntry> dictionary = new ArrayList<>();
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    if (QueryParser.existUnknowns(solution.get("lem")))
                        continue;
                    dictionary.add(new GrammaticalDictionaryEntry(data.getLemma(),
                            QueryParser.convertQToSpeechPart(solution.get("myPart")),
                            QueryParser.convertQToWord(solution.get("gender")),
                            new ArrayList<>(Collections.singleton(
                                    new WordForms(QueryParser.ifNull(solution.get("wordLabel")),
                                            QueryParser.convertFeaturesQToWord(solution.get("lem")))))));
                }
                List<GrammaticalDictionaryEntry> empties = dictionary.stream()
                        .filter(word -> word.getGender().compareTo("") == 0).collect(Collectors.toList());
                List<GrammaticalDictionaryEntry> notEmpties = dictionary.stream()
                        .filter(word -> word.getGender().compareTo("") != 0).collect(Collectors.toList());

                List<WordForms> wordForms = new ArrayList<>();
                if (empties.size() > 0 && notEmpties.size() > 0) {
                    wordForms.addAll(dictionary.stream()
                            .filter(word -> word.getGender().compareTo("") == 0)
                            .flatMap(word -> word.getWordForms().stream()).collect(Collectors.toList()));
                    ((GrammaticalDictionaryEntry) data).setPart(empties.get(0).getPart());
                    ((GrammaticalDictionaryEntry) data).setGender(empties.get(0).getGender());
                } else {
                    wordForms.addAll(dictionary.stream()
                            .flatMap(word -> word.getWordForms().stream()).collect(Collectors.toList()));
                    ((GrammaticalDictionaryEntry) data).setPart(dictionary.get(0).getPart());
                    ((GrammaticalDictionaryEntry) data).setGender(dictionary.get(0).getGender());
                }
                ((GrammaticalDictionaryEntry) data).setWordForms(wordForms);
                break;
            }
            case "Толковый": {
                throwNotFoundException(result);
                List<String> senses = new ArrayList<>();
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    senses.add(QueryParser.ifNull(solution.get("senseLabel")));
                }
                ((ExplanatoryDictionaryEntry) data).setSenses(senses);
                break;
            }
            case "Переводной": {
                throwNotFoundException(result);
                List<Translation> definitions = new ArrayList<>();
                int definitionIdx = -1;
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    String definition = QueryParser.ifNull(solution.get("conceptLabel"));
                    Pair<String, String> pair = new Pair<>(
                            QueryParser.ifNullWithReplace(solution.get("languageLabel")),
                            QueryParser.ifNull(solution.get("translationLemmaLabel")));
                    // if the same definition
                    if (definitions.stream().anyMatch(
                            def -> def.getDefinition().compareTo(definition) == 0)) {
                        // if another lang and translation
                        if (definitions.stream().noneMatch(def ->
                                def.getTranslations().stream().anyMatch(translationPair ->
                                        translationPair.getLanguage().compareTo(pair.getLanguage()) == 0))) {
                            definitions.get(definitionIdx).getTranslations().add(pair);
                        }
                    } else {
                        definitionIdx++;
                        definitions.add(new Translation(definition, new ArrayList<>()));
                        definitions.get(definitionIdx).getTranslations().add(pair);
                    }
                }
                ((TranslationDictionaryEntry) data).setDefinitions(definitions);
                break;
            }
            default: {
                throwNotFoundException(result);
                List<List<String>> allFeatures = new ArrayList<>(new ArrayList<>());
                while (result.hasNext()) {
                    QuerySolution solution = result.next();
                    List<String> recordFeatures = new ArrayList<>();
                    for (String field : dictionaryQueryData.getQueryFields()) {
                        recordFeatures.add(QueryParser.ifNullWithReplace(solution.get(field)));
                    }
                    allFeatures.add(recordFeatures);
                }
//                for (List<String> feature : allFeatures) {
//                    System.out.println(String.join(", ", feature));
//                }
                ((CustomDictionaryEntry) data).setFeatures(allFeatures);
                break;
            }
        }
        return data;
//        } catch (QueryBuildException qbe) {
//            qbe.getStackTrace();
////            QueryBuildException is exception for all exceptions during query execution construction.
//            System.out.println("QueryBuildException");
//        } catch (QueryCancelledException qce) {
//            qce.getStackTrace();
////            Indicate that a query execution has been cancelled and the operation can 't be called
//            System.out.println("QueryCancelledException");
//        } catch (QueryExecException qee) {
//            qee.getStackTrace();
////            QueryExecException indicates a condition encountered during query evaluation.
//            System.out.println("QueryExecException");
//        } catch (QueryFatalException qfe) {
//            qfe.getStackTrace();
////            QueryFatalException is such that the query aborts do to some
////            problem(this might be an internal error or something in the way the query builds or executes).
//            System.out.println("QueryFatalException");
//        } catch (QueryParseException qpe) {
//            qpe.getStackTrace();
//            System.out.println("QueryParseException");
//        }
    }

}
