package sparql;

import java.util.List;

public class DictionaryQueryData {
    private String dictionaryType;
    private String templatePath;
    private String queryPathForAll;
    private String queryTextForAll;
    private String queryPathForOne;
    private String queryTextForOne;
    private String queryFieldForAll;
    private List<String> queryFields;

    public String getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(String dictionaryType) {
        this.dictionaryType = dictionaryType;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getQueryPathForAll() {
        return queryPathForAll;
    }

    public void setQueryPathForAll(String queryPathForAll) {
        this.queryPathForAll = queryPathForAll;
    }

    public String getQueryTextForAll() {
        return queryTextForAll;
    }

    public void setQueryTextForAll(String queryTextForAll) {
        this.queryTextForAll = queryTextForAll;
    }

    public String getQueryPathForOne() {
        return queryPathForOne;
    }

    public void setQueryPathForOne(String queryPathForOne) {
        this.queryPathForOne = queryPathForOne;
    }

    public String getQueryTextForOne() {
        return queryTextForOne;
    }

    public void setQueryTextForOne(String queryTextForOne) {
        this.queryTextForOne = queryTextForOne;
    }

    public String getQueryFieldForAll() {
        return queryFieldForAll;
    }

    public void setQueryFieldForAll(String queryFieldForAll) {
        this.queryFieldForAll = queryFieldForAll;
    }

    public List<String> getQueryFields() {
        return queryFields;
    }

    public void setQueryFields(List<String> queryFields) {
        this.queryFields = queryFields;
    }
}
