package utils.dictionaryComparator;

import dictionary.entry.IDictionaryEntry;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class DictionaryComparator implements Comparator<IDictionaryEntry> {
    private Collator spCollator = Collator.getInstance(new Locale("ru", "RU"));

    public int compare(IDictionaryEntry e1, IDictionaryEntry e2) {
        return spCollator.compare(e1.getLemma(), e2.getLemma());
    }
}
