package utils.velocity;

import dictionary.custom.CustomDictionaryEntry;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.readQuery.ReadQuery;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static frontend.controllers.MainViewController.currentQueryData;

public class VelocityFileWork {

    private static String rewriteIfList(Document document) {
        String template = document.toString();
        String tag = template.contains("ol") ? "ol" : "ul";
        Elements list = document.select(tag);
        String partBeforeForeach = template.substring(0, template.indexOf("#foreach"));
        String partAfterForeach = template.substring(template.indexOf("#foreach"));
        int trStartIdx = partAfterForeach.indexOf("<li>");
        int trEndIdx = partAfterForeach.lastIndexOf("</li>") + 6;
        partAfterForeach = partAfterForeach.replace(partAfterForeach.substring(trStartIdx, trEndIdx), "");
        StringBuilder newTemplate = new StringBuilder("#foreach ($some in $someList)\n\t\t");
        newTemplate.append("#set($size = $some.size() - 1)\n\t\t");
        newTemplate.append("#foreach ($index in [0..$size])\n\t\t");
        for (Element value : list) {
            Elements li = value.select("li");
            for (Element element : li) {
                newTemplate.append(String.format("%s\n\t\t\t", element));
            }
        }
        partAfterForeach = partAfterForeach.replace("#foreach", newTemplate.toString());
        partAfterForeach = partAfterForeach.replace(String.format("</%s>",tag), String.format("#end\n\t#end\n</%s>",tag));
        return partBeforeForeach.concat(partAfterForeach);
    }

    private static String rewriteIfTable(Document document) {
        Elements trs = document.select("table").select("tr");
        String template = document.toString();
        String partBeforeForeach = template.substring(0, template.indexOf("#foreach"));
        int countDelete = partBeforeForeach.split("<tr>").length - 1;
        if (countDelete > 0) {
            trs.subList(0, countDelete).clear();
        }
        String partAfterForeach = template.substring(template.indexOf("#foreach"));
        int trStartIdx = partAfterForeach.indexOf("<tr>");
        int trEndIdx = partAfterForeach.lastIndexOf("</tr>") + 6;
        partAfterForeach = partAfterForeach.replace(partAfterForeach.substring(trStartIdx, trEndIdx), "");
        StringBuilder newTemplate = new StringBuilder("#foreach ($some in $someList)\n\t\t");
        newTemplate.append("#foreach ($index in [0..$some.size()])\n\t\t<tr>\n\t\t\t");
        for (int i = 0; i < trs.size(); i++) {
            Elements tds = trs.get(i).select("td");
            if (i == 0) {
                newTemplate.append(String.format("#if($index == %d)\n\t\t\t", i));
            } else {
                newTemplate.append(String.format("#elseif($index == %d)\n\t\t\t", i));
            }
            int idx = 0;
            for (int j = 0; j < tds.size(); j++) {
                Element td = tds.get(j);
                if (td.toString().contains("$some.get($index)")) {
                    idx = j;
                } else {
                    newTemplate.append(String.format("%s\n\t\t\t", tds.get(j)));
                }
            }
            newTemplate.append(String.format("%s\n\t\t\t", tds.get(idx)));
        }
        newTemplate.append("#end\n\t\t");
        newTemplate.append("</tr>");
        partAfterForeach = partAfterForeach.replace("#foreach", newTemplate.toString());
        partAfterForeach = partAfterForeach.replace("</tbody>", "#end\n\t#end\n</tbody>");
        return partBeforeForeach.concat(partAfterForeach);
    }

    private static String rewriteVelocityFile() throws IOException {
        String template = ReadQuery.readFile(currentQueryData.getTemplatePath());
        String fieldForAll = currentQueryData.getQueryFieldForAll();
        template = template.replace(fieldForAll, String.format("$%s", fieldForAll));
        for (String field : currentQueryData.getQueryFields()) {
            template = template.replace(field, "$some.get($index)");
        }
        Document document = Jsoup.parse(template);
        if (document.select("table").size() > 0) {
            template = rewriteIfTable(document);
        } else if (document.select("ul").size() > 0 || document.select("ol").size() > 0) {
            template = rewriteIfList(document);//template.replace("</body>", "#end\n#end\n</body>");
        }
        return template;
    }

    private static String[] getDirectoryPathAndFileName(String path) {
        int endIdx = path.lastIndexOf(File.separator);
        String pathToTemplateDirectory = path.substring(0, endIdx);
        String templateName = path.substring(endIdx + 1);
        return new String[]{pathToTemplateDirectory, templateName};
    }

    private static String[] createNewTemplate(String pathToCustomTemplateDirectory,
                                              String customTemplateName) throws IOException {
        String pathToFullTemplateDirectory = String.format("%s/fullTemplates", pathToCustomTemplateDirectory);
        String pathToFullTemplate = String.format("%s/full_%s", pathToFullTemplateDirectory, customTemplateName);
        Path path = Paths.get(pathToFullTemplateDirectory);
        Files.createDirectories(path);
        File sourceFile = new File(pathToFullTemplate);
        FileUtils.writeStringToFile(sourceFile, rewriteVelocityFile(), StandardCharsets.UTF_8);
        return getDirectoryPathAndFileName(pathToFullTemplate);
    }


    public static String getTemplate(CustomDictionaryEntry dictionary) throws ResourceNotFoundException,
            ParseErrorException, MethodInvocationException, IOException {
        StringWriter writer = new StringWriter();
        String[] customTemplatePathData = getDirectoryPathAndFileName(currentQueryData.getTemplatePath());
        String[] pathToFullTemplate = createNewTemplate(
                customTemplatePathData[0], customTemplatePathData[1]);
        Properties p = new Properties();
        p.setProperty("file.resource.loader.path", pathToFullTemplate[0]);
        Velocity.init(p);
        VelocityContext context = new VelocityContext();
        context.put(currentQueryData.getQueryFieldForAll(), dictionary.getLemma());
        context.put("someList", dictionary.getFeatures());
        Template template = Velocity.getTemplate(pathToFullTemplate[1], "UTF-8");
        template.merge(context, writer);

//        String[] customTemplatePathData = getDirectoryPathAndFileName(dictionary.getTemplatePath());
//        Properties p = new Properties();
//        p.setProperty("file.resource.loader.path", customTemplatePathData[0]);
//        Velocity.init(p);
//        VelocityContext context = new VelocityContext();
//        context.put(dictionary.getQueryFieldForAll(), dictionary.getLemma());
//        context.put("someList", dictionary.getFeatures());
//        Template template = Velocity.getTemplate(customTemplatePathData[1], "UTF-8");
//        template.merge(context, writer);

        System.out.println(writer.toString());
        return writer.toString();
    }

}
