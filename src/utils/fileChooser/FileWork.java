package utils.fileChooser;

import app.App;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class FileWork {

    public static String createFileName(String pathToDirectory, String dictType) {
        return String.format("%s/%s.json", pathToDirectory, dictType);
    }

    public static void setDirectoryChooserSettings(DirectoryChooser directoryChooser, String propertyKey) {
        directoryChooser.setTitle(App.getProperties().getProperty(propertyKey));
    }

    public static void setFileChooserSettings(FileChooser fileChooser, String propertyKey) {
        fileChooser.setTitle(App.getProperties().getProperty(propertyKey));
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("JSON files", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);
    }

}
