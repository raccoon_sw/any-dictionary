package utils.wordsOrder;

import dictionary.entry.IDictionaryEntry;
import utils.dictionaryComparator.DictionaryComparator;

import java.util.List;

public class WordsOrder {

    public static void setLexicographicalOrder(List<IDictionaryEntry> results) {
        results.sort(new DictionaryComparator());
    }

    private static void reverseLemma(List<IDictionaryEntry> results) {
        for (IDictionaryEntry data : results) {
            StringBuilder lemma = new StringBuilder(data.getLemma());
            data.setLemma(lemma.reverse().toString());
        }
    }

    public static void setInverseOrder(List<IDictionaryEntry> results) {
        reverseLemma(results);
        results.sort(new DictionaryComparator());
        reverseLemma(results);
    }
}
