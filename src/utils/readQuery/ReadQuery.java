package utils.readQuery;

import com.google.gson.Gson;
import dictionary.Dictionary;
import sparql.DictionaryQueryData;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadQuery {

    private static String getAbsolutePath(String mayBeRelativePath) {
        return FileSystems.getDefault().getPath(mayBeRelativePath).normalize().toAbsolutePath().toString();
    }

    private static void setRelativePathToAbsolute(DictionaryQueryData dictionaryQueryData, boolean isCustom) {
        if (isCustom) {
            dictionaryQueryData.setTemplatePath(getAbsolutePath(dictionaryQueryData.getTemplatePath()));
        }
        dictionaryQueryData.setQueryPathForAll(getAbsolutePath(dictionaryQueryData.getQueryPathForAll()));
        dictionaryQueryData.setQueryPathForOne(getAbsolutePath(dictionaryQueryData.getQueryPathForOne()));
    }

    public static String readFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
    }

    public static <T> T readObjectWithGson(String jsonPath, Class<?> className) throws IOException {
        String jsonText = readFile(jsonPath);
        return new Gson().fromJson(jsonText, (Type) className);
    }

    public static DictionaryQueryData setQueryPaths(String jsonPath, boolean isCustom) throws IOException {
        DictionaryQueryData dictionaryQueryData = readObjectWithGson(jsonPath, DictionaryQueryData.class);
        setRelativePathToAbsolute(dictionaryQueryData, isCustom);
        dictionaryQueryData.setQueryTextForAll(readFile(dictionaryQueryData.getQueryPathForAll()));
        dictionaryQueryData.setQueryTextForOne(readFile(dictionaryQueryData.getQueryPathForOne()));
        return dictionaryQueryData;
    }

    public static List<Dictionary> getDictionaryData(String directoryPath, boolean isCustom) throws IOException {
        List<Dictionary> queriesData = new ArrayList<>();
        Stream<Path> paths = Files.walk(Paths.get(directoryPath), 1);
        List<String> jsonFilesPaths = paths.filter(Files::isRegularFile).map(Path::toString)
                .filter(json -> !json.contains("DS_STORE")).collect(Collectors.toList());
        for (String jsonPath : jsonFilesPaths) {
            queriesData.add(new Dictionary(setQueryPaths(jsonPath, isCustom)));
        }
        return queriesData;
    }
}
