package templates.custom;

import dictionary.custom.CustomDictionaryEntry;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import utils.velocity.VelocityFileWork;

import java.io.IOException;

public class CustomTemplate {

    public static String getDictionaryEntry(CustomDictionaryEntry dictionary) throws ResourceNotFoundException,
            ParseErrorException, MethodInvocationException, IOException {
        return VelocityFileWork.getTemplate(dictionary);
    }

}
