package templates.grammatical.partsOfSpeech;

import dictionary.grammatical.WordForms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Noun {

    private static List<String> allCases = Arrays.asList("именительный падеж", "родительный падеж", "дательный падеж",
            "винительный падеж", "творительный падеж", "предложный падеж", "местный падеж");

    static List<String> getAllCases() {
        return allCases;
    }

    static List<WordForms> getCase(List<WordForms> wordForms, String caseName) {
        return wordForms.stream().filter(case0 -> case0.getLemmas().contains(caseName))
                .collect(Collectors.toList());
    }

    static String setSingularsOrPluralsForNouns(boolean isSingular, List<WordForms> caseWordForms) {
        return caseWordForms.stream().filter(word -> word.getLemmas()
                .contains(isSingular ? "единственное число" : "множественное число"))
                .map(WordForms::getWord).collect(Collectors.joining(",\n"));
    }

    private static List<List<String>> getCasesForNouns(List<WordForms> wordForms) {
        List<List<String>> cases = new ArrayList<>();
        for (String caseName : allCases) {
            List<WordForms> caseWordForms = getCase(wordForms, caseName);
            if (caseWordForms.size() > 0)
                cases.add(Arrays.asList(caseName,
                        setSingularsOrPluralsForNouns(true, caseWordForms),
                        setSingularsOrPluralsForNouns(false, caseWordForms)));
        }
        return cases;
    }

    public static String setNounTable(List<WordForms> wordForms) {
        StringBuilder str = new StringBuilder("<table border=\"1\" cellspacing=\"0\"\n" +
                "  cellpadding=\"10\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th bgcolor=\"#EEF9FF\"></th>\n" +
                "      <th bgcolor=\"#EEF9FF\">единственное число</th>\n" +
                "      <th bgcolor=\"#EEF9FF\">множественное число</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n");
        for (List<String> caseData : getCasesForNouns(wordForms)) {
            String table = String.format("<tr>\n" +
                    "  <td bgcolor=\"#EEF9FF\">%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "</tr>\n", caseData.get(0), caseData.get(1), caseData.get(2));
            str.append(table);
        }
        str.append("  </tbody></table>\n");
        return str.toString();
    }

}
