package templates.explanatory;

import dictionary.explanatory.ExplanatoryDictionaryEntry;

import java.util.List;

public class ExplanatoryTemplate {

    private static String getOrderedSensesList(List<String> senses) {
        StringBuilder htmlPage = new StringBuilder("<ol>\n");
        for (String sense : senses) {
            htmlPage.append(String.format("<li>%s</li>\n", sense));
        }
        return htmlPage.append("</ol>\n").toString();
    }

    public static String getDictionaryEntry(ExplanatoryDictionaryEntry data) {
        String cntSenses = data.getSenses().size() > 1 ? "Значения" : "Значение";
        return String.format("<html lang=\"en\">\n" +
                        "<h4>Семантические свойства</h4>\n" +
                        "<p>%s<br></p>\n" +
                        "<p>%s:<br></p>\n" +
                        "<p>%s</p>\n" +
                        "</html>",
                data.getLemma(), cntSenses, getOrderedSensesList(data.getSenses()));
    }

}
