package templates.translation;

import dictionary.translation.Pair;
import dictionary.translation.Translation;
import dictionary.translation.TranslationDictionaryEntry;

import java.util.List;

public class TranslationTemplate {

    private static String setTranslationTable(List<Translation> definitions) {
        StringBuilder htmlPage = new StringBuilder("<table border=\"1\" cellspacing=\"0\"\n" +
                "  cellpadding=\"10\">\n" +
                "  <tbody>\n");
        for (Translation definition : definitions) {
            String table = String.format("<tr>\n" +
                            "  <td bgcolor=\"#EEF9FF\">Определение</td>\n" +
                            "  <td colspan=\"2\">%s</td>\n" +
                            "</tr>\n" +
                            "<tr>\n" +
                            String.format("  <td bgcolor=\"#EEF9FF\" valign=\"top\" rowspan=\"%d\">Перевод</td>",
                                    definition.getTranslations().size()),
                    definition.getDefinition());
            htmlPage.append(table);
            for (int i = 0; i < definition.getTranslations().size(); i++) {
                Pair<String, String> pair = definition.getTranslations().get(i);
                table = i == 0 ? "" : "<tr>\n";
                table += String.format("  <td>%s</td>\n" +
                        "  <td>%s</td>\n" +
                        "</tr>\n", pair.getLanguage(), pair.getTranslation());
                htmlPage.append(table);
            }
        }
        htmlPage.append("  </tbody></table>\n");
        return htmlPage.toString();
    }

    public static String getDictionaryEntry(TranslationDictionaryEntry data) {
        return String.format("<html lang=\"en\">\n" +
                        "<h4>Перевод</h4>\n" +
                        "<p>%s<br></p>\n" +
                        "<p>%s</p>\n" +
                        "</html>",
                data.getLemma(), setTranslationTable(data.getDefinitions()));
    }

}
