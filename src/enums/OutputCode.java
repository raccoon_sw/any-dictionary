package enums;

public enum OutputCode {
    QueryForAllHasLoaded,
    QueryForOneHasLoaded,
    QueryNoWordsForAllFound,
    QueryNoWordForOneFound,
    ErrorDataForAllNotFound,
    ErrorDataForOneNotFound,
    DictionaryTypeIsLoaded,
    DictionaryIsLoaded,
    DictionarySaved
}
