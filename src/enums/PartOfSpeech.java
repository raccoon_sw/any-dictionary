package enums;

public enum PartOfSpeech {
    Noun,
    Verb,
    Adjective,
    Adverb,
    None
}
