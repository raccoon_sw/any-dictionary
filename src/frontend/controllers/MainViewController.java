package frontend.controllers;

import app.App;
import com.google.gson.*;
import dictionary.Dictionary;
import dictionary.custom.CustomDictionaryEntry;
import dictionary.entry.IDictionaryEntry;
import dictionary.explanatory.ExplanatoryDictionaryEntry;
import dictionary.grammatical.GrammaticalDictionaryEntry;
import dictionary.translation.TranslationDictionaryEntry;
import enums.OutputCode;
import enums.WordOrderType;
import exceptions.EmptyEntryException;
import exceptions.MyException;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.jena.atlas.json.JsonParseException;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sparql.DictionaryQueryData;
import sparql.QueryParser;
import sparql.SparqlService;
import utils.fileChooser.FileWork;
import utils.readQuery.ReadQuery;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class MainViewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private MenuBar mbLoadQuery;

    @FXML
    private MenuItem miSave;

    @FXML
    private MenuItem miSaveAs;

    @FXML
    private ListView<IDictionaryEntry> listDictionary;

    @FXML
    private WebView webWordTemplate;

    @FXML
    private TextField tfWord;

    @FXML
    private Button btnCreateDictionary;

    @FXML
    private ComboBox<String> cbDictionaryType;

    @FXML
    private TextArea taLog;

    private Stage primaryStage;
    private List<Dictionary> dictionaries;
    public static DictionaryQueryData currentQueryData;
    private int count = 0;
    private static StringBuilder forLog = new StringBuilder();

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private IDictionaryEntry setDictionaryEntry(String dictType, String word) {
        IDictionaryEntry dictionary;
        switch (dictType) {
            case "Грамматический": {
                dictionary = new GrammaticalDictionaryEntry(word);
                break;
            }
            case "Толковый": {
                dictionary = new ExplanatoryDictionaryEntry(word);
                break;
            }
            case "Переводной": {
                dictionary = new TranslationDictionaryEntry(word);
                break;
            }
            default: {
                dictionary = new CustomDictionaryEntry(word);
                break;
            }
        }
        return dictionary;
    }

    private void setLogTextWithFormat(OutputCode code, String dictType, String word) {
        switch (code) {
            case ErrorDataForAllNotFound: {
                forLog.append(String.format(
                        App.getProperties().getProperty("errorDataForAllNotFound"), dictType));
                break;
            }
            case ErrorDataForOneNotFound: {
                forLog.append(String.format(
                        App.getProperties().getProperty("errorDataForOneNotFound"), word, dictType));
                break;
            }
            case QueryForAllHasLoaded: {
                forLog.append(String.format(
                        App.getProperties().getProperty("queryForAllHasLoaded"), dictType, Integer.valueOf(word)));
                break;
            }
            case QueryForOneHasLoaded: {
                forLog.append(String.format(
                        App.getProperties().getProperty("queryForOneHasLoaded"), dictType, word));
                break;
            }
            case QueryNoWordsForAllFound: {
                forLog.append(String.format(
                        App.getProperties().getProperty("queryNoWordsForAllFound"), dictType));
                break;
            }
            case QueryNoWordForOneFound: {
                forLog.append(String.format(
                        App.getProperties().getProperty("queryNoWordForOneFound"), dictType, word));
                break;
            }
            case DictionaryTypeIsLoaded: {
                forLog.append(String.format(
                        App.getProperties().getProperty("dictionaryTypeIsLoaded"), dictType, word));
                break;
            }
            case DictionaryIsLoaded: {
                forLog.append(String.format(
                        App.getProperties().getProperty("dictionaryIsLoaded"), dictType, word));
                break;
            }
            case DictionarySaved: {
                forLog.append(String.format(
                        App.getProperties().getProperty("dictionarySaved"), dictType, word));
                break;
            }
        }
        taLog.setText(forLog.append("\n").toString());
    }

    private void setLogText(String propertyKey) {
        forLog.append(App.getProperties().getProperty(propertyKey));
        taLog.setText(forLog.append("\n").toString());
    }

    private IDictionaryEntry getResponseForOne(IDictionaryEntry data, DictionaryQueryData queryData,
                                               String newWord, boolean isSearch) throws MyException {
        IDictionaryEntry newData;
        try {
            newData = SparqlService.getResponseForOne(data, queryData, isSearch ? newWord : "");
            count++;
        } catch (QueryExceptionHTTP qeh) {
            setLogText("errorNoInternetConnection");
            throw new MyException();
        } catch (JsonParseException jpe) {
            setLogText("errorImpossibleToParseQuery");
            throw new MyException();
        } catch (IllegalStateException ise) {
            setLogTextWithFormat(OutputCode.ErrorDataForOneNotFound,
                    queryData.getDictionaryType().toLowerCase(), newWord);
            throw new MyException();
        } catch (Exception e) {
            setLogText("errorUnknown");
            throw new MyException();
        }
        return newData;
    }

    private Dictionary getResponseForAll() throws MyException {
        Dictionary data;
        try {
            data = SparqlService.getResponseForAll(currentQueryData, WordOrderType.Lexicographical);
        } catch (QueryExceptionHTTP qeh) {
            setLogText("errorNoInternetConnection");
            throw new MyException();
        } catch (JsonParseException jpe) {
            setLogText("errorImpossibleToParseQuery");
            throw new MyException();
        } catch (IllegalStateException ise) {
            setLogTextWithFormat(OutputCode.ErrorDataForAllNotFound,
                    currentQueryData.getDictionaryType().toLowerCase(), "");
            throw new MyException();
        } catch (Exception e) {
            setLogText("errorUnknown");
            throw new MyException();
        }
        return data;
    }

    private boolean isDictionaryTypeExists(String dictType) {
        return this.dictionaries.stream().anyMatch(dictionary ->
                dictionary.getType().equals(dictType));
    }

    private Dictionary createDictionaryFromQuery() throws MyException {
        Dictionary dictionaryOld = findDictionaryByType(currentQueryData.getDictionaryType());
        int idx = dictionaries.indexOf(dictionaryOld);
        Dictionary dictionaryNew = getResponseForAll();
        dictionaries.set(idx, dictionaryNew);
        setDisableSaveButtons(false);
        return dictionaryNew;
    }

    private Dictionary findDictionaryByType(String dictType) {
        return dictionaries.stream().filter(data ->
                data.getType().equals(dictType)).collect(Collectors.toList()).get(0);
    }

    private Optional<Dictionary> findDictionaryWithEntries(String dictType) {
        return dictionaries.stream().filter(dictionary -> dictionary.getType().equals(dictType)
                && dictionary.getDictionaryEntries().size() > 0).findFirst();
    }

    private Optional<IDictionaryEntry> findDictionaryEntry(Dictionary dictionary, String lemma) {
        return dictionary.getDictionaryEntries().stream()
                .filter(entry -> entry.getLemma().equals(lemma)).findAny();
    }

    private IDictionaryEntry createDictionaryEntry(Dictionary dictionary, String newWord) throws MyException {
        Optional<IDictionaryEntry> dictionaryEntry = findDictionaryEntry(dictionary, newWord);
        if (!dictionaryEntry.isPresent()) {
            setLogTextWithFormat(OutputCode.QueryNoWordForOneFound, dictionary.getType(), newWord);
            throw new MyException();
        }
        try {
            switch (dictionary.getType()) {
                case "Грамматический": {
                    GrammaticalDictionaryEntry entry = (GrammaticalDictionaryEntry) dictionaryEntry.get();
                    if (entry.getWordForms().size() == 0)
                        throw new EmptyEntryException();
                    break;
                }
                case "Толковый": {
                    ExplanatoryDictionaryEntry entry = (ExplanatoryDictionaryEntry) dictionaryEntry.get();
                    if (entry.getSenses().size() == 0)
                        throw new EmptyEntryException();
                    break;
                }
                case "Переводной": {
                    TranslationDictionaryEntry entry = (TranslationDictionaryEntry) dictionaryEntry.get();
                    if (entry.getDefinitions().size() == 0)
                        throw new EmptyEntryException();
                    break;
                }
                default: {
                    CustomDictionaryEntry entry = (CustomDictionaryEntry) dictionaryEntry.get();
                    if (entry.getFeatures().size() == 0)
                        throw new EmptyEntryException();
                    break;
                }
            }
        } catch (EmptyEntryException e) {
            return getResponseForOne(setDictionaryEntry(dictionary.getType(), newWord),
                    currentQueryData, newWord, false);
        }
        return dictionaryEntry.get();

    }

    private void setNewDictionaryEntries(Dictionary dictionary, IDictionaryEntry newEntry) {
        List<IDictionaryEntry> dictionaryEntries = dictionary.getDictionaryEntries();
        Optional<IDictionaryEntry> dictionaryEntry = findDictionaryEntry(dictionary, newEntry.getLemma());
        if (dictionaryEntry.isPresent()) {
            int idx = dictionaryEntries.indexOf(dictionaryEntry.get());
            dictionaryEntries.set(idx, newEntry);
        }
    }

    @FXML
    void btnCreateDictionaryOnAction(ActionEvent event) {
        String dictType = currentQueryData.getDictionaryType();
        Dictionary data;

        try {
            Optional<Dictionary> optionalData = findDictionaryWithEntries(dictType);
            if (optionalData.isPresent()) {
                data = optionalData.get();
            } else {
                data = createDictionaryFromQuery();
            }
        } catch (MyException e) {
            return;
        }

        // if get the whole dictionary
        if (tfWord.getText().equals("")) {
            setLogText("queryIsLoading");
            listDictionary.getItems().clear();
            if (data.getDictionaryEntries().size() == 0) {
                setLogTextWithFormat(OutputCode.QueryNoWordsForAllFound, dictType.toLowerCase(), "");
            } else {
                listDictionary.getItems().addAll(data.getDictionaryEntries());
                setLogTextWithFormat(OutputCode.QueryForAllHasLoaded,
                        dictType.toLowerCase(), String.valueOf(data.getDictionaryEntries().size()));
            }
        } else {
            String newWord = tfWord.getText();
            IDictionaryEntry dictionaryEntry;
            try {
                dictionaryEntry = createDictionaryEntry(data, newWord);
                setNewDictionaryEntries(data, dictionaryEntry);
                // if needed search
                if (listDictionary.getItems().size() > 1) {
                    listDictionary.getItems().clear();
                    listDictionary.getItems().addAll(data.getDictionaryEntries());
                    Platform.runLater(() -> scrollToSelectedItem(dictionaryEntry));
                } else {
                    setLogText("queryIsLoading");
                    listDictionary.getItems().clear();
                    setLogTextWithFormat(OutputCode.QueryForOneHasLoaded, dictType.toLowerCase(), newWord);
                    listDictionary.getItems().add(dictionaryEntry);
                    setLogTextWithFormat(OutputCode.QueryForOneHasLoaded, dictType.toLowerCase(), newWord);
                }
                tfWord.setText("");
            } catch (MyException e) {
                return;
            }
        }
        listDictionary.getSelectionModel().select(0);
    }

    private String getFilePathFromChooser(boolean isSaving, String propertyKey) throws MyException {
        FileChooser fileChooser = new FileChooser();
        FileWork.setFileChooserSettings(fileChooser, propertyKey);
        File file = fileChooser.showOpenDialog(primaryStage);
        if (file != null) {
            if (isSaving)
                setLogText("dictionarySaving");
            return file.getAbsolutePath();
        } else {
            setLogText("errorJsonFileIsNotChosen");
            throw new MyException();
        }
    }

    private List<IDictionaryEntry> getDictionaryEntriesFromJson(JsonArray jsonArray, Class<?> className) {
        List<IDictionaryEntry> dictionaryEntries = new ArrayList<>();
        for (JsonElement element : jsonArray) {
            dictionaryEntries.add(new Gson().fromJson(element, (Type) className));
        }
        return dictionaryEntries;
    }

    private Dictionary setNotCustomDictionaryData(JsonObject object, String dictType) {
        Dictionary dictionary = new Dictionary();
        dictionary.setType(dictType);
        JsonArray jsonArray = object.get("dictionary").getAsJsonArray();
        switch (dictType) {
            case "Грамматический": {
                dictionary.setDictionaryEntries(
                        getDictionaryEntriesFromJson(jsonArray, GrammaticalDictionaryEntry.class));
                break;
            }
            case "Толковый": {
                dictionary.setDictionaryEntries(
                        getDictionaryEntriesFromJson(jsonArray, ExplanatoryDictionaryEntry.class));
                break;
            }
            case "Переводной": {
                dictionary.setDictionaryEntries(
                        getDictionaryEntriesFromJson(jsonArray, TranslationDictionaryEntry.class));
                break;
            }
        }
        return dictionary;
    }

    private Dictionary setCustomDictionaryData(JsonObject object) {
        DictionaryQueryData queryData = new DictionaryQueryData();
        queryData.setDictionaryType(object.get("dictionaryType").getAsString());
        queryData.setTemplatePath(object.get("templatePath").getAsString());
        queryData.setQueryFieldForAll("lemma");
        JsonArray jsonArray = object.get("dictionary").getAsJsonArray();
        Dictionary dictionary = new Dictionary();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject featuresObject = jsonArray.get(i).getAsJsonObject();
            String lemma = featuresObject.get("lemma").getAsString();

            CustomDictionaryEntry entry = new CustomDictionaryEntry(lemma);
            JsonArray jsonFeaturesArray = featuresObject.get("features").getAsJsonArray();
            if (i == 0)
                queryData.setQueryFields(new ArrayList<>(jsonFeaturesArray.get(i).getAsJsonObject().keySet()));
            List<List<String>> features = new ArrayList<>();
            for (int j = 0; j < jsonFeaturesArray.size(); j++) {
                JsonObject feature = jsonFeaturesArray.get(j).getAsJsonObject();
                List<String> featureRecord = new ArrayList<>();
                for (String featureName : queryData.getQueryFields()) {
                    featureRecord.add(feature.get(featureName).getAsString());
                }
                features.add(featureRecord);
            }
            entry.setFeatures(features);
            dictionary.getDictionaryEntries().add(entry);
        }
        dictionary.setQueryData(queryData);
        return dictionary;
    }

    private Dictionary readFromJson(JsonObject object) throws MyException {
        Dictionary dictionary;
        String dictType = object.get("dictionaryType").getAsString();
        switch (dictType) {
            case "Грамматический":
            case "Толковый":
            case "Переводной": {
                dictionary = setNotCustomDictionaryData(object, dictType);
                break;
            }
            default: {
                dictionary = setCustomDictionaryData(object);
                if (this.dictionaries.stream().anyMatch(data -> data.getType().equals(dictionary.getType()))) {
                    throw new MyException();
                }
                break;
            }
        }
        return dictionary;
    }

    @FXML
    void btnImportDictionaryOnAction() {
        try {
            String filePath = getFilePathFromChooser(false, "jsonImportDictionaryPath");
            JsonObject object = ReadQuery.readObjectWithGson(filePath, JsonObject.class);
            Dictionary dictionaryNew = readFromJson(object);
            Optional<Dictionary> dictionaryOld = findDictionaryWithEntries(dictionaryNew.getType());
            if (dictionaryOld.isPresent()) {
                int idx = dictionaries.indexOf(dictionaryOld.get());
                dictionaries.set(idx, dictionaryNew);
            } else {
                dictionaries.add(dictionaryNew);
            }
            setComboBox(false, dictionaryNew.getType());
            listDictionary.getItems().clear();
            listDictionary.getItems().addAll(dictionaryNew.getDictionaryEntries());
            Platform.runLater(() -> scrollToSelectedItem(dictionaryNew.getDictionaryEntries().get(0)));
            setLogTextWithFormat(OutputCode.DictionaryIsLoaded, dictionaryNew.getType(), "");
        } catch (IOException e) {
            setLogText("errorJsonFileIsWrong");
        } catch (MyException e) {
            setLogText("errorDictionaryTypeExist");
        } catch (Exception e) {
            setLogText("errorJsonFileIsNotFull");
            //
        }
    }

    private Dictionary getAllLemmas(String dictType) throws Exception {
        Dictionary dictionary = new Dictionary();
        if (dictType.equals(currentQueryData.getDictionaryType()) && listDictionary.getItems().size() > 0) {
            dictionary.setDictionaryEntries(listDictionary.getItems());
        } else {
            dictionary = getResponseForAll();
        }
        return dictionary;
    }

    @SuppressWarnings("unchecked")
    private JSONObject getNotCustomDictionaryData(Dictionary dictionary) throws Exception {
        List<IDictionaryEntry> savedData = new ArrayList<>();
        List<IDictionaryEntry> getLemmas = getAllLemmas(dictionary.getType()).getDictionaryEntries()
                .stream().limit(22).collect(Collectors.toList());
        for (IDictionaryEntry data : getLemmas) {
//            if (data.getLemma().equals("большой") || data.getLemma().equals("аангич"))
//                Thread.sleep(30000);
            if (count != 0 && count % 3 == 0) {
                System.out.println(data.getLemma() + ", i = " + count);
                Thread.sleep(60000);
            }
            IDictionaryEntry newData = createDictionaryEntry(dictionary, data.getLemma());
            savedData.add(newData);
        }
        JSONObject object = new JSONObject();
        object.put("dictionaryType", dictionary.getType());
        object.put("dictionary", savedData);
        return object;
    }

    @SuppressWarnings("unchecked")
    private JSONObject getCustomDictionaryData(Dictionary dictionary) throws MyException {
        JSONObject object = new JSONObject();
        object.put("dictionaryType", dictionary.getType());
        object.put("templatePath", dictionary.getQueryData().getTemplatePath());

        JSONArray array = new JSONArray();
        for (IDictionaryEntry dictionaryEntry : dictionary.getDictionaryEntries().stream().limit(1).collect(Collectors.toList())) {
            CustomDictionaryEntry entry = (CustomDictionaryEntry)
                    createDictionaryEntry(dictionary, dictionaryEntry.getLemma());
            JSONObject featuresObject = new JSONObject();
            featuresObject.put(dictionary.getQueryData().getQueryFieldForAll(), entry.getLemma());
            JSONArray featuresRecordArray = new JSONArray();
            for (List<String> oneFeaturesRecord : entry.getFeatures()) {
                JSONObject featuresRecordObj = new JSONObject();
                for (int i = 0; i < oneFeaturesRecord.size(); i++) {
                    featuresRecordObj.put(dictionary.getQueryData().getQueryFields().get(i), oneFeaturesRecord.get(i));
                }
                featuresRecordArray.add(featuresRecordObj);
            }
            featuresObject.put("features", featuresRecordArray);
            array.add(featuresObject);
        }
        object.put("dictionary", array);
        return object;
    }

    void writeToJson(Dictionary dictionary, Writer writer) throws Exception {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        switch (dictionary.getType()) {
            case "Грамматический":
            case "Толковый":
            case "Переводной": {
                gson.toJson(getNotCustomDictionaryData(dictionary), writer);
                break;
            }
            default: {
                gson.toJson(getCustomDictionaryData(dictionary), writer);
                break;
            }
        }
    }

    @FXML
    void btnSaveDictionaryOnAction() {
        String dictType = cbDictionaryType.getSelectionModel().getSelectedItem();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        FileWork.setDirectoryChooserSettings(directoryChooser, "jsonSaveDictionaryPath");
        File directory = directoryChooser.showDialog(primaryStage);
        if (directory != null) {
            count = 0;
            setLogText("dictionarySaving");
            Dictionary dictionary = findDictionaryByType(dictType);
            String filePath = FileWork.createFileName(directory.getAbsolutePath(), dictType);
            try (Writer writer = Files.newBufferedWriter(Paths.get(filePath))) {
                writeToJson(dictionary, writer);
                setLogTextWithFormat(OutputCode.DictionarySaved, dictType, "");
            } catch (IOException e) {
                setLogText("errorFileIsNotCreated");
            } catch (Exception e) {
                //
            }
        } else {
            setLogText("errorDirectoryIsNotChosen");
        }
    }

    @FXML
    void btnSaveDictionaryAsOnAction() {
        String dictType = cbDictionaryType.getSelectionModel().getSelectedItem();
        Dictionary dictionary = findDictionaryByType(dictType);
        try {
            count = 0;
            String filePath = getFilePathFromChooser(true, "jsonSaveDictionaryAsPath");
            try (Writer writer = Files.newBufferedWriter(Paths.get(filePath))) {
                writeToJson(dictionary, writer);
                setLogTextWithFormat(OutputCode.DictionarySaved, dictType, "");
            } catch (IOException e) {
                setLogText("errorJsonFileIsWrong");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (MyException e) {
            //
        }
    }

    private void loadQuery() {
        try {
            String filePath = getFilePathFromChooser(false, "jsonQueryPath");
            DictionaryQueryData queryData = ReadQuery.setQueryPaths(filePath, true);
            if (isDictionaryTypeExists(queryData.getDictionaryType())) {
                setLogText("errorDictionaryTypeExist");
            }
            dictionaries.add(new Dictionary(queryData));
            setComboBox(false, queryData.getDictionaryType());
            setLogTextWithFormat(OutputCode.DictionaryTypeIsLoaded, queryData.getDictionaryType(), "");
        } catch (IOException e) {
            setLogText("errorJsonFileIsWrong");
        } catch (Exception e) {
            //
        }
    }

    @FXML
    void initialize() {
        assert mbLoadQuery != null : "fx:id=\"mbLoadQuery\" was not injected: check your FXML file 'mainView.fxml'.";
        assert miSave != null : "fx:id=\"miSave\" was not injected: check your FXML file 'mainView.fxml'.";
        assert miSaveAs != null : "fx:id=\"miSaveAs\" was not injected: check your FXML file 'mainView.fxml'.";
        assert listDictionary != null : "fx:id=\"dictionary\" was not injected: check your FXML file 'mainView.fxml'.";
        assert webWordTemplate != null : "fx:id=\"taWordTemplate\" was not injected: check your FXML file 'mainView.fxml'.";
        assert tfWord != null : "fx:id=\"tfWord\" was not injected: check your FXML file 'mainView.fxml'.";
        assert btnCreateDictionary != null : "fx:id=\"btnCreateDictionary\" was not injected: check your FXML file 'mainView.fxml'.";
        assert cbDictionaryType != null : "fx:id=\"cbDictionaryType\" was not injected: check your FXML file 'mainView.fxml'.";
        assert taLog != null : "fx:id=\"taLog\" was not injected: check your FXML file 'mainView.fxml'.";

        try {
            this.dictionaries = new ArrayList<>();
            QueryParser.setQueryParser();
            this.dictionaries = ReadQuery.getDictionaryData(
                    App.getProperties().getProperty("queriesDirectoryPath"), false);
            setComboBox(true, "");
        } catch (IOException e) {
            forLog.append(e.getMessage()).append("\n");
            taLog.setText(forLog.toString());
        }

        WebEngine webEngine = webWordTemplate.getEngine();

        // add custom query loader button
        Label menuLabel = new Label(App.getProperties().getProperty("downloadOwnQuery"));
        menuLabel.setOnMouseClicked(event -> loadQuery());
        Menu loadQueryMenuButton = new Menu();
        loadQueryMenuButton.setGraphic(menuLabel);
        mbLoadQuery.getMenus().add(0, loadQueryMenuButton);

        cbDictionaryType.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                currentQueryData = findDictionaryByType(newSelection).getQueryData();
                Optional<Dictionary> optionalData = findDictionaryWithEntries(newSelection);
                if (optionalData.isPresent()) {
                    setDisableSaveButtons(false);
                } else {
                    setDisableSaveButtons(true);
                }
            }
        });

        listDictionary.setCellFactory(lv -> factoryForList());

        listDictionary.getItems().addListener((InvalidationListener) observable -> {
            if (listDictionary.getItems().isEmpty()) {
                webEngine.loadContent("");
            }
        });

        listDictionary.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null) {
                webEngine.loadContent("");
            } else {
                IDictionaryEntry data = listDictionary.getSelectionModel().getSelectedItem();
                String dictType = currentQueryData.getDictionaryType();
                String newWord = tfWord.getText();
                try {
                    switch (dictType) {
                        case "Грамматический": {
                            if (((GrammaticalDictionaryEntry) data).getWordForms().size() > 0) {
                                webEngine.loadContent(data.getDictionaryTemplate());
                                return;
                            }
                            break;
                        }
                        case "Толковый": {
                            if (((ExplanatoryDictionaryEntry) data).getSenses().size() > 0) {
                                webEngine.loadContent(data.getDictionaryTemplate());
                                return;
                            }
                            break;
                        }
                        case "Переводной": {
                            if (((TranslationDictionaryEntry) data).getDefinitions().size() > 0) {
                                webEngine.loadContent(data.getDictionaryTemplate());
                                return;
                            }
                            break;
                        }
                        default: {
                            if (((CustomDictionaryEntry) data).getFeatures().size() > 0) {
                                webEngine.loadContent(data.getDictionaryTemplate());
                                return;
                            }
                            break;
                        }
                    }

//                    System.out.println(dictType);
                    try {
                        IDictionaryEntry newEntry = getResponseForOne(data,
                                currentQueryData, newWord, true);
                        setNewDictionaryEntries(findDictionaryByType(dictType), newEntry);
                        webEngine.loadContent(newEntry.getDictionaryTemplate());
                    } catch (Exception e) {
                        //
                    }

                } catch (ResourceNotFoundException rnfe) { // couldn't find the template
                    setLogText("errorTemplateFileNotFound");
                } catch (ParseErrorException pee) { // syntax error: problem parsing the template
                    setLogText("errorTemplateHasWrongFormat");
                } catch (MethodInvocationException mie) {// something invoked in the template
                    setLogText("errorUnknownTemplate");
                } catch (Exception e) { // read file exception
                    setLogText("errorTemplateRead");
                }
            }
        });
    }

    private void setDisableSaveButtons(boolean flag) {
        miSave.setDisable(flag);
        miSaveAs.setDisable(flag);
    }

    private void setComboBox(boolean isAll, String dictType) {
        if (isAll) {
            cbDictionaryType.getItems().addAll(dictionaries.stream().map(Dictionary::getType).collect(Collectors.toList()));
            cbDictionaryType.getSelectionModel().select(0);
            setDisableSaveButtons(true);
            currentQueryData = findDictionaryByType(cbDictionaryType.getSelectionModel().getSelectedItem()).getQueryData();
        } else {
            cbDictionaryType.getItems().add(dictType);
            cbDictionaryType.getSelectionModel().select(dictType);
        }
    }

    private ListCell<IDictionaryEntry> factoryForList() {
        return new ListCell<IDictionaryEntry>() {
            @Override
            protected void updateItem(IDictionaryEntry item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText("");
                } else {
                    setText(item.getLemma());
                }
            }
        };
    }

    private void scrollToSelectedItem(IDictionaryEntry dictionary) {
        int idx = listDictionary.getItems().indexOf(dictionary);
        listDictionary.scrollTo(idx);
        listDictionary.getSelectionModel().select(idx);
    }

}
