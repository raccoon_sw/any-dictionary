PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX bd: <http://www.bigdata.com/rdf#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
SELECT DISTINCT ?lemma
WHERE {
  ?l a ontolex:LexicalEntry ;
    dct:language wd:Q7737 ;
    wikibase:lexicalCategory ?myPart ;
    wikibase:lemma ?lemma ;
    ontolex:sense ?sense ;
    ontolex:sense/skos:definition ?concept .
  VALUES ?myPart { wd:Q1084 wd:Q24905 wd:Q34698 wd:Q380057 }
  ?sense wdt:P5972 ?meaning .
  ?translex ontolex:sense ?meaning .
  ?translex wikibase:lemma ?translemma .
  ?translex dct:language ?lang .
  FILTER (LANG(?concept)= 'ru') .
}